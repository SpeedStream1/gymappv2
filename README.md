Version 1.0.0.01
- Creation of the project
- First MVP includes:
    - Login, Signup, Main, Rutina and Seguimiento activities
    - Connection with DB to get info and set values
    - Get info required by user
        - Account & gym info
        - Rutina
        - ¿Qué comer?
        - Seguimiento

-------------------------------------------------

REFERENCIAS:
- http://www.hermosaprogramacion.com/2015/12/consumir-un-servicio-web-rest-desde-android/
- https://devtroce.com/2012/01/24/como-invocar-a-un-web-service-desde-androi/
- https://docs.oracle.com/cd/B10002_01/generic.903/b10004/javaservices.htm
- https://stackoverflow.com/questions/25782582/send-put-request-in-android-to-rest-api
- https://www.programacion.com.py/moviles/android/acceso-a-web-service-rest-en-android
- https://code.i-harness.com/en/q/124f041
- https://code.google.com/archive/p/ksoap2-android/wikis/CodingTipsAndTricks.wiki
- https://docs.microsoft.com/en-us/xamarin/xamarin-forms/data-cloud/consuming/rest
- https://www.javatpoint.com/android-web-service
