package com.example.asantos.gymappv2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Login extends AppCompatActivity {
    public static final String LOG_TAG = Login.class.getSimpleName();
    private static final int REQUEST_SIGNUP = 0;

    //WS_Operaciones operaciones = new WS_Operaciones();
    private String msgError = "Hello there...";
    private EditText email, password;
    private Button buttonLogin, buttonFb;
    private TextView signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText)findViewById(R.id.input_email);
        password = (EditText)findViewById(R.id.input_password);
        buttonLogin = (Button)findViewById(R.id.btn_login);
        buttonFb = (Button)findViewById(R.id.btnFacebook);
        signUp = (TextView)findViewById(R.id.link_signup);

        buttonLogin.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        login();
                    }
                }
        );

        signUp.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), Signup.class);
                        startActivityForResult(intent, REQUEST_SIGNUP);
                        finish();
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                }
        );
    }

    private void login(){
        Log.d(LOG_TAG, "Login");

        if(!validate()) {
            loginFailed();
            return;
        }

        buttonLogin.setEnabled(false);
        buttonFb.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Ingresando...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        boolean resLogin = true;
                        //boolean resLogin = operaciones.IniciarSesion(email.getText().toString(), password.getText().toString());
                        if (resLogin)
                            loginSuccess();
                        else
                            loginFailed();
                        progressDialog.dismiss();
                    }
                }, 1000);
    }
    private boolean validate(){
        boolean valido = true;

        String _email = email.getText().toString();
        String _password = password.getText().toString();
        if(_email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(_email).matches()){
            msgError = "Correo electrónico inválido";
            valido = false;
        }
        if(_password.isEmpty()){
            msgError = "Proporcione una contraseña";
            valido = false;
        }
        return true;
    }
    private void loginFailed(){
        Toast.makeText(getBaseContext(), "OOOPS! " + msgError, Toast.LENGTH_LONG).show();
        buttonFb.setEnabled(true);
        buttonLogin.setEnabled(true);
    }
    private void loginSuccess(){
        Intent intent = new Intent(this, GestorInicio.class);
        intent.putExtra("Login", true);
        //intent.putExtra("IDUsuario", User.IDUser);
        //intent.putExtra("UserName", User.userName);
        //intent.putExtra("IDInstructor", User.IDInstructor);
        //intent.putExtra("IDGym", User.IDGym);
        //intent.putExtra("AlCorriente", User.AlCorriente);

        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }
}
