package com.example.asantos.gymappv2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class Fragment1_Inicio extends Fragment {

    private LinearLayout Configuracion;
    private LinearLayout Avisos;
    private LinearLayout Rutina;
    private LinearLayout Menu;
    private NavigationView NavView;

    public Fragment1_Inicio() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Configuracion = (LinearLayout) getView().findViewById(R.id.SeccionConfiguracion);
        Avisos = (LinearLayout) getView().findViewById(R.id.SeccionAvisos);
        Rutina = (LinearLayout) getView().findViewById(R.id.SeccionRutina);
        Menu = (LinearLayout)  getView().findViewById(R.id.menuPrincipal);
        NavView = (NavigationView) getView().findViewById(R.id.navview);


        Menu.setOnClickListener(new View.OnClickListener() {
            Fragment fr = null;
            boolean fragmentTransaction = false;

            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.SeccionRutina:
                        fr = new Fragment3_RutinaDia();
                        fragmentTransaction = true;
                        // TODO: Asignar control a rutina del día
                        // NavView.setCheckedItem(R.id.menu);
                        break;
                    case R.id.SeccionConfiguracion:
                        // TODO: Crear control para configuracion
                        // NavView.setCheckedItem(R.id.menu);
                        break;
                }

                if (fragmentTransaction){
                    
                }
            }
        });


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment1_inicio, container, false);

    }
}