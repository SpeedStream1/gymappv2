package com.example.asantos.gymappv2;

import java.io.IOException;
import android.util.Log;
import java.net.URL;
import static com.example.asantos.gymappv2.Login.LOG_TAG;

public class WS_Operaciones {
    public static final String WS_Url = "http://localhost:57139/ServicioMicrositio.asmx";
    Connections connections = new Connections();

    public boolean IniciarSesion(String username, String password) {
        URL url = connections.createUrl(WS_Url + "?op=Login&user="+username+"&password="+password);
        String jsonResponse = "";
        try {
            jsonResponse = connections.makeHttpRequest(url);
            return  true;
        }
        catch (IOException e){
            Log.d(LOG_TAG, "Get failed for unknown reason");
            return false;
        }
    }

    public boolean CrearCuenta(String nombre, String apellido, String email, String password) {
        Usuario usuario = new Usuario();
        usuario.setNewUsuario(nombre, apellido, email, password);
        URL url = connections.createUrl(WS_Url + "?op=SignIn&");
        String jsonResponse = "";
        try {
            jsonResponse = connections.makeHttpRequest(url);
            return  true;
        }
        catch (IOException e){
            Log.d(LOG_TAG, "Get failed for unknown reason");
            return false;
        }
    }
}
