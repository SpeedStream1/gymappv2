package com.example.asantos.gymappv2;

public class Usuario {
    private Integer mIdUsr = 0;
    private Integer mIdCoach = 0;
    private String mNombre;
    private String mApellidos;
    private String mEmail;
    private String mPassword;

    public Usuario(){}

    public void setNewUsuario(String nombre, String apellidos, String email, String password){
        mNombre = nombre;
        mApellidos = apellidos;
        mEmail = email;
        mPassword = password;
    }

    public void setUsuario(Integer idUsr, Integer idCoach, String nombre, String apellidos, String email, String password){
        mIdUsr = idUsr;
        mIdCoach = idCoach;
        setNewUsuario(nombre,apellidos,email,password);
    }

    public String getNombre(){return mNombre;}
    public String getApellidos(){return mApellidos;}
    public String getEmail(){return mEmail;}
    public Integer getIdCoach(){return mIdCoach;}
    public Integer getIdUsr(){return mIdUsr;}
}