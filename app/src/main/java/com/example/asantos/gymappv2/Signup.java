package com.example.asantos.gymappv2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Signup extends AppCompatActivity{
    public static final String LOG_TAG = Signup.class.getSimpleName();
    String msgError = "";
    //WS_Operaciones operaciones = new WS_Operaciones();
    EditText txNombre, txApellidos, txEmail, txPassword, txPassword2;
    Button btnCrear, btnFb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    private void crearCuenta(){
        //Log.d(LOG_TAG, WS_Operaciones"Registrar usuario nuevo: " + txNombre);
        txNombre = (EditText)findViewById(R.id.signupNombre);
        txApellidos = (EditText)findViewById(R.id.signupApellidos);
        txEmail = (EditText)findViewById(R.id.signupEmail);
        txPassword = (EditText)findViewById(R.id.signupPassword);
        txPassword2 = (EditText)findViewById(R.id.signupPassword2);
        btnCrear = (Button) findViewById(R.id.signupCrearCuenta);
        btnFb = (Button) findViewById(R.id.signupFacebook);

        if (!validar()){
            signUpFailed();
            return;
        }

        btnFb.setEnabled(false);
        btnCrear.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Signup.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Ingresando...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        //boolean resLogin = operaciones.CrearCuenta();
                        //ValidateSignUp(resLogin);
                        progressDialog.dismiss();
                    }
                }, 10000);
    }

    private void signUpFailed(){
        Toast.makeText(getBaseContext(), "OOOPS! " + msgError, Toast.LENGTH_LONG).show();
        btnFb.setEnabled(true);
        btnCrear.setEnabled(true);
    }

    private boolean validar(){
        if(txNombre.getText().toString().isEmpty())
            msgError = "Proporcione un nombre.";
        if(txApellidos.getText().toString().isEmpty())
            msgError = "Proporcione un apellido.";
        if(txApellidos.getText().toString().isEmpty())
            msgError = "Proporcione un apellido.";
        if(txEmail.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(txEmail.getText().toString()).matches())
            msgError = "Proporcione un email válido.";
        if(txPassword.getText().toString().isEmpty())
            msgError = "Proporcione una contraseña válida.";
        if(txPassword.getText().toString().length() < 6)
            msgError = "Su contraseña es muy corta.";
        if(txPassword2.getText().toString().isEmpty())
            msgError = "Proporcione la contraseña válida.";
        if(txPassword2.getText().toString() != txPassword.getText().toString())
            msgError = "Las contraseñas no coinciden";

        return !msgError.isEmpty();
    }
    private void ValidateSignUp(Boolean signUpSuccess){
        if (signUpSuccess){
            //Account created
            Intent intent = new Intent(this, GestorInicio.class);
            //Enviar idUsuario
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        }
        else {
            //Account not created
            Toast.makeText(getBaseContext(), "OOOPS! " + msgError, Toast.LENGTH_LONG).show();
            btnFb.setEnabled(true);
            btnCrear.setEnabled(true);
        }
    }
}
